shader_type spatial;

// Gonkee's fog shader for Godot 3 - full tutorial https://youtu.be/QEaTsz_0o44
// If you use this shader, I would prefer it if you gave credit to me and my channel

uniform vec3 color = vec3(1.0, 0.01, 0.01);
uniform int OCTAVES = 4;

float rand(vec2 coord){
	return fract(cos(dot(coord, vec2(-23.14069263277926, 2.665144142690225)) * 44621.12) * -71257.0) + fract(sin(dot(coord, vec2(2.665144142690225, -23.14069263277926)) * -71257.0) * 44621.0);
}

float noise(vec2 coord){
	vec2 i = floor(coord);
	vec2 f = fract(coord);

	// 4 corners of a rectangle surrounding our point
	float a = rand(i);
	float b = rand(i + vec2(1.0, 0.0));
	float c = rand(i + vec2(0.0, 1.0));
	float d = rand(i + vec2(1.0, 1.0));

	vec2 cubic = f * f * (3.0 - 2.0 * f);

	return mix(a, b, cubic.x) + (c - a) * cubic.y * (1.0 - cubic.x) + (d - b) * cubic.x * cubic.y;
}

float fbm(vec2 coord){
	float value = 0.0;
	float scale = 0.4;

	for(int i = 0; i < OCTAVES; i++){
		value += noise(vec2(noise(coord),noise(-coord))) * scale;
		coord *= 2.0;
		scale *= 0.5;
	}
	return value;
}

void fragment() {
	vec2 coord = UV * 45.0;

	vec2 motion = vec2( fbm(coord + vec2(TIME/-5.0, TIME/7.0)) );

	float final = fbm(coord + vec2(TIME/-17.0, TIME/19.0) + motion);

	ALBEDO = color;
	ALPHA = final;
}