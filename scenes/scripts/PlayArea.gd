extends Spatial
onready var boundary_box = vr.get_boundary_oriented_bounding_box()
var fence_scene = preload("res://scenes/Fence.tscn")
var puzzle_size = Vector3(1.0, 4.0, 0.0) #how wide, tall, fat
export var safety_margin = 0.1
export(PoolStringArray) var puzzlesPaths = []
var loadedPuzzles = []
export var emptyPuzzle = preload("res://scenes/childs/puzzles/EmptyPuzzle.tscn")
var x_size
var z_size

func _ready():
	for path in puzzlesPaths:
		loadedPuzzles.append(load(path))
	calculate_space_size()
	
	generate_obstacles()
	
		
export(vr.AXIS) var move_up_down = vr.AXIS.LEFT_JOYSTICK_Y;
func _process(delta):
	var dy = vr.get_controller_axis(move_up_down)
	if (Input.is_key_pressed(KEY_W)): dy = -1
	elif (Input.is_key_pressed(KEY_A)): dy = 1
	update_lift_ovement(dy, delta)

func generate_obstacles():
	var fitting_fences_x = x_size/puzzle_size.x;
	var remainder_x = fmod(fitting_fences_x, 1.0)
	fitting_fences_x -= remainder_x
	var fence_initial_spacer = Vector3(0.5, puzzle_size.y/2.0, 0)
	if(fmod(fitting_fences_x, 2.0)==0):
		fitting_fences_x = (x_size/puzzle_size.x)-1
		fence_initial_spacer = Vector3(1, puzzle_size.y/2, 0)
	fence_initial_spacer.x += (fmod(x_size, puzzle_size.x)/2)
	for i in range(fitting_fences_x):
		var distance_from_origin = Vector3( -x_size/2.0, 0, -z_size/2.0)
		var fence_position = Vector3(i*(puzzle_size.x), 0, 0)
		add_child(create_fence(distance_from_origin + fence_position + fence_initial_spacer, 0))

		distance_from_origin = Vector3( -x_size/2.0, 0, z_size/2.0)
		add_child(create_fence(distance_from_origin + fence_position + fence_initial_spacer, 0))

	var fitting_fences_z = z_size/puzzle_size.x;
	var remainder_z = fmod(fitting_fences_z, 1.0)
	fitting_fences_z -= remainder_z
	fence_initial_spacer = Vector3(0, puzzle_size.y/2.0, 0.5)
	if(fmod(fitting_fences_z, 2.0)==0):
		fitting_fences_z = (z_size/puzzle_size.x)-1;
		fence_initial_spacer = Vector3(0, puzzle_size.y/2, 1)

	fence_initial_spacer.z += fmod(z_size, puzzle_size.x)/2
	for i in range(fitting_fences_z):
		var distance_from_origin = Vector3( x_size/2.0, 0, -z_size/2.0)
		var fence_position = Vector3(0, 0, i*(puzzle_size.x))
		add_child(create_fence(distance_from_origin + fence_position + fence_initial_spacer, PI/2.0))

		distance_from_origin = Vector3( -x_size/2.0, 0, -z_size/2.0)
		add_child(create_fence(distance_from_origin + fence_position + fence_initial_spacer, PI/2.0))

func calculate_space_size():
	transform = boundary_box[0]
	x_size = boundary_box[1].x*2.0
	z_size = boundary_box[1].z*2.0
	
	$WallsCube.mesh.size.x = x_size 
	$WallsCube.mesh.size.z = z_size

func create_fence(fence_tranlsation, y_rotation = 0):
	var fence_instance = fence_scene.instance()
	var puzzlePScene = loadedPuzzles.pop_front()
	if(!puzzlePScene):
		puzzlePScene = emptyPuzzle
	fence_instance.get_node("PuzzleOrigin").add_child(puzzlePScene.instance())
	fence_instance.translation = fence_tranlsation
	fence_instance.rotate_y(y_rotation)
	return fence_instance


func _on_Lever_childLeverActivated(dy, delta):
	update_lift_ovement(dy, delta)
	
var distance = 0
func update_lift_ovement(dy, delta):
	distance += dy*delta
	if(distance < -5):
		translation.y = 0
		distance = 0
	elif(distance > 0):
		translation.y = -5
		distance = -5
	translation.y += dy*delta
