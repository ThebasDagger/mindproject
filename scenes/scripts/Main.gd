extends Spatial

func _process(_dt):
	pass

func _ready():
	randomize()
	vr.initialize();
	
	vr.scene_switch_root = self;

	# Always advertise Godot a bit in the beggining
	#if (vr.inVR): vr.switch_scene("res://demo_scenes/GodotSplash.tscn", 0.0, 0.0);
	vr.switch_scene("res://scenes/Main.tscn", 0.0, 0.0);

	vr.log_info("  Tracking space is: %d" % vr.get_tracking_space());
	vr.log_info(str("  get_boundary_oriented_bounding_box is: ", vr.get_boundary_oriented_bounding_box()));
	vr.log_info("Engine.target_fps = %d" % Engine.target_fps);

