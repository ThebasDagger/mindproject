extends Spatial

var buttonSize = Vector3( 0.4, 0.4, 0.1)
var boardButtonMatrix = []
var boardState = []
var solutionSteps = []
var automatedSolution = false
export var boardMatrixSize = Vector2( 3, 3)
export var toggleMatrixEffect = [[0, 1, 0],[1, 1, 1],[0, 1, 0]]
export var randomizeToggleMatrixEffect = true
export var shuffleBoard = true
export var autoSolve = true
export var autoSolveTimer = 15
var buttonScene = preload("res://scenes/childs/Button.tscn")
var isValid = false
var isShowingHints = false
var buttonTranslationScale = [
						[[0.0,0.0,1.0,1.0],[0.0,0.0,1.0,1.0],[-0.348,0.0,1.0,1.0],[-0.077,0.0,1.0,1.0],[0.3315,0.0,1.0,1.0],[0.0,0.0,1.0,1.0]],
						[[0.0,0.315,1.0,1.0],[0.0,0.315,1.0,1.0],[-0.348,0.315,1.1,1.65],[-0.077,0.244,1.48,1.0],[0.3315,0.3215,1.2,1.65],[0.0,0.315,1.0,1.0]],
						[[0.0,0.0,1.0,1.0],[0.0,0.0,1.0,1.0],[-0.388,0.045,0.545,0.785],[0.0915,0.044,1.19,0.88],[0.3425,0.088,1.11,0.55],[0.0,0.0,1.0,1.0]],
						[[0.0,-1.0,1.0,1.0],[0.0,-1.0,1.0,1.0],[-0.441,-0.156,1.78,1.1],[0.007,-0.119,1.48,0.65],[0.365,-0.084,1.32,0.96],[0.0,-1.0,1.0,1.0]],
						[[0.0,0.0,1.0,1.0],[0.0,0.0,1.0,1.0],[-0.348,0.0,1.0,1.0],[-0.077,0.0,1.0,1.0],[0.3315,0.0,1.0,1.0],[0.0,0.0,1.0,1.0]],
						[[0.0,0.0,1.0,1.0],[0.0,0.0,1.0,1.0],[-0.348,0.0,1.0,1.0],[-0.077,0.0,1.0,1.0],[0.3315,0.0,1.0,1.0],[0.0,0.0,1.0,1.0]]
					]
func _ready():
	toggleMatrixEffect = toggleMatrixEffect.duplicate(true)
	create_button_board()
	if(randomizeToggleMatrixEffect):
		randomize_toggle_matrix_effect()
	if(shuffleBoard):
		shuffle_board()
	if(autoSolve):
		timed_solve_puzzle()
	showBoardHint()
	isShowingHints = true

func create_button_board():
	for i in range(boardMatrixSize.x):
		boardButtonMatrix.append([])
		boardState.append([])
		for j in range(boardMatrixSize.y):
			var button = create_button(i, j)
			boardButtonMatrix[i].append(button)
			boardState[i].append(true)

func randomize_toggle_matrix_effect():
	var numOfTogglers = 0
	while(numOfTogglers <= 1):
		numOfTogglers = 0
		for i in range(3):
			for j in range(3):
				toggleMatrixEffect[i][j] = randi() % 2
				numOfTogglers += toggleMatrixEffect[i][j]

func create_button(var x, var y):
	var button = buttonScene.duplicate(true).instance()
	button.coordinates = Vector2(x, y)
	button.translation = translation_for_button(x, y)
	button.scale = scale_for_button(x, y)
	add_child(button)
	return button

func shuffle_board():
	while(is_valid_solution() and solutionSteps.size() < boardMatrixSize.length()):
		for i in range(boardMatrixSize.length_squared()):
			button_de_activated(Vector2(randi()%int(boardMatrixSize.x), randi()%int(boardMatrixSize.y)), false)
	finishBoard()

func button_de_activated(coordinates, sound_effect = true):
	if(isShowingHints):
		clearBoardHint()
		return
	if(sound_effect and isValid):
		return
	for i in range(3):
		for j in range(3):
			if(i ==1 and j == 1):
				boardButtonMatrix[coordinates.x][coordinates.y].de_activate(false, sound_effect)
				boardState[coordinates.x][coordinates.y] = boardButtonMatrix[coordinates.x][coordinates.y].activated
			elif(toggleMatrixEffect[2-j][i]):
				if(coordinates.x+i-1 >= 0 and coordinates.x+i-1 < boardMatrixSize.x):
					if(coordinates.y+j-1 >= 0 and coordinates.y+j-1 < boardMatrixSize.y):
						boardButtonMatrix[coordinates.x+i-1][coordinates.y+j-1].de_activate(true, sound_effect)
						boardState[coordinates.x+i-1][coordinates.y+j-1] = boardButtonMatrix[coordinates.x+i-1][coordinates.y+j-1].activated
	if(sound_effect and is_valid_solution()):
		isValid = true
		finishBoard()
		yield(get_tree().create_timer(1),"timeout")
		$AudioStreamPlayer3D.play()
	if(!automatedSolution):
		add_to_solution_steps_matrix(coordinates, boardState.duplicate(true))

func is_valid_solution():
	for i in range(boardMatrixSize.x):
		for j in range(boardMatrixSize.y):
			if(!boardButtonMatrix[i][j].activated):
				return false
	return true

func add_to_solution_steps_matrix(pressedButtonCoordinates, boardState = []):
	for i in range(solutionSteps.size()):
		if(are_board_states_equal(solutionSteps[i][1], boardState)):
			solutionSteps.resize(i+1)
			return
	solutionSteps.append([pressedButtonCoordinates, boardState])

func are_board_states_equal(boardStateA, boardStateB):
	for i in range(boardMatrixSize.x):
		for j in range(boardMatrixSize.y):
			if(boardStateA[i][j] != boardStateB[i][j]):
				return false
	return true
	
func timed_solve_puzzle():
	yield(get_tree().create_timer(autoSolveTimer),"timeout")
	if(isValid):
		return
	automatedSolution = true
	button_de_activated(solutionSteps[0][0], true)
	for i in range(solutionSteps.size(), 0, -1):
		yield(get_tree().create_timer(1.5),"timeout")
		button_de_activated(solutionSteps[i-1][0], true)

func translation_for_button(x, y):
	x = 1+x - int(boardMatrixSize.x/2)
	y = y - int(boardMatrixSize.y/2)
	var boardPosition = buttonTranslationScale.size()/2-1
	return Vector3( buttonTranslationScale[boardPosition+y][boardPosition+x][0], buttonTranslationScale[boardPosition+y][boardPosition+x][1], 0)

func scale_for_button(x, y):
	x = 1+x - int(boardMatrixSize.x/2)
	y = y - int(boardMatrixSize.y/2)
	var boardPosition = buttonTranslationScale.size()/2-1
	return Vector3( buttonTranslationScale[boardPosition+y][boardPosition+x][2], buttonTranslationScale[boardPosition+y][boardPosition+x][3], 0.05)

func clearBoardHint():
	isShowingHints = false
	for i in range(boardMatrixSize.x):
		for j in range(boardMatrixSize.y):
			boardButtonMatrix[i][j].process_de_activation()
	
func finishBoard():
	yield(get_tree().create_timer(0.5),"timeout")
	var finishedColor = boardButtonMatrix[0][0].finishedColor
	for i in range(boardMatrixSize.x):
		for j in range(boardMatrixSize.y):
			boardButtonMatrix[i][j].change_button_material_color(finishedColor, false, false)
			
func showBoardHint():
	yield(get_tree().create_timer(0.5),"timeout")
	var hintColor = boardButtonMatrix[0][0].hintColor
	for i in range(3):
		for j in range(3):
			if(toggleMatrixEffect[2-j][i]):
				boardButtonMatrix[i][j].change_button_material_color(hintColor, false, false)
		
