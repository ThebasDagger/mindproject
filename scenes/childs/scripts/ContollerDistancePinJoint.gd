extends PinJoint
var tooDistant = false
var tooDistantTimer = 0
func _process(delta):
	var distance = get_node(get_node_a()).global_transform.origin.distance_to(get_node(get_node_b()).global_transform.origin)
	if(tooDistant):
		get_node(get_node_b()).global_transform.origin = get_node(get_node_a()).global_transform.origin
		tooDistant = false
	elif(distance>0.1):
		tooDistantTimer += delta
		if(tooDistantTimer > 2.0):
			tooDistant = true
	else:
		tooDistantTimer = 0
