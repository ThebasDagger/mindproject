tool
extends Area

export var activated = false
onready var active = true
export var activeColor = Color("30caba7a")
export var deactiveColor = Color("50404040")
export var justActivedColor = Color("804f492d")
export var hintColor = Color("40ff6e67")
export var finishedColor = Color("00ffffff")
var coordinates = Vector2(0, 0)
var delay = true
var sound = true
export var timeToReactivate = 0.5
onready var triggeredPath = preload("res://sound_effects/Switch-SoundBible.com-350629905.wav")
onready var chainTriggeredPath = preload("res://sound_effects/Click On-SoundBible.com-1697535117.wav")


func _ready():
	connect("body_entered",self,"handle_collision")
	active = !activated
	
func handle_collision(body):
	if(elapsedTime >= timeToReactivate and body.has_method("is_controller_collider") and body.is_controller_collider()):
		elapsedTime = 0
		inform_parent()
	
func _input_event(camera, event, pos, normal, shape):
	if(elapsedTime >= timeToReactivate and event is InputEventMouseButton):
		if (event.is_action_pressed("mouse_left_button")):
			elapsedTime = 0
			inform_parent()

var elapsedTime = timeToReactivate
func _process(delta):
	elapsedTime += delta
	if(active != activated):
		process_de_activation()

func inform_parent():
	get_parent().button_de_activated(coordinates)

func de_activate(with_delay = true, with_sound = true):
	activated = !activated
	delay = with_delay
	sound = with_sound

func process_de_activation():
	if(activated):
		change_button_material_color(activeColor, delay, sound)
	else:
		change_button_material_color(deactiveColor, delay, sound)
	active = activated
	sound = true
	delay = true

func change_button_material_color(targetColor, with_delay, with_sound):
	$Tween.remove_all()
	var material = $ButtonMesh.get("material/0").duplicate()
	$ButtonMesh.set("material/0", material)
	var delay = int(with_delay) * randf()/1.5
	var easeFunction = Tween.EASE_OUT_IN
	$AudioStreamPlayer3D.stream = triggeredPath
	if(with_delay):
		easeFunction = Tween.EASE_IN_OUT
		$AudioStreamPlayer3D.stream = chainTriggeredPath
	else:
		$Tween.interpolate_property(material, "albedo_color", material.get("albedo_color"), justActivedColor, 0.1, Tween.TRANS_EXPO, easeFunction, 0)
		$Tween.interpolate_property(material, "albedo_color", justActivedColor, material.get("albedo_color"), 0.1, Tween.TRANS_EXPO, easeFunction, 0.1)
	$Tween.interpolate_property(material, "albedo_color", material.get("albedo_color"), targetColor, 0.5, Tween.TRANS_SINE, easeFunction, delay+0.2)
	if(with_sound):
		$Tween.interpolate_callback($AudioStreamPlayer3D, delay+0.2, "play")
	$Tween.start()

