extends Spatial

var areaPointA = Vector3(-1.0, -1.0, 0.0)
var areaPointB = Vector3(1.0, 1.0, 0.0)
var arrowOriginSpacing = Vector3(0.3, 0.3, 0)
var randomizerLimits = Vector3(0.1, 0.1, 0)
onready var arrowOriginMesh = $ArrowOriginMesh

func _ready():
	var areaWideness = abs(areaPointA.x)+abs(areaPointB.x)
	var areaTallness = abs(areaPointA.y)+abs(areaPointB.y)
	for i in range(areaPointA.x, areaWideness/arrowOriginSpacing.x - abs(areaPointA.x)):
		for j in range(areaPointA.y, areaTallness/arrowOriginSpacing.y - abs(areaPointA.y)):
			var arrowOrigin = arrowOriginMesh.duplicate(true)
			arrowOriginMesh.translation = Vector3(i*arrowOriginSpacing.x+(randf()*randomizerLimits.x-randomizerLimits.x/2), j*arrowOriginSpacing.y+(randf()*randomizerLimits.y-randomizerLimits.y/2), 0)
			arrowOriginMesh.visible = true
			add_child(arrowOrigin)
	$ArrowOriginMesh.queue_free()
